PYTHON?=python3
PACKAGE_NAME:="$(subst _,-,$(shell grep 'name = .*' setup.cfg | cut -d ' ' -f3-))"
PACKAGE_DIR="$(subst -,_,$(PACKAGE_NAME))"

env-test:
	@if ! test -d env ; \
	then echo "No virtualenv 'env' found"; \
	exit 1; \
	fi

env-build:
	@echo
	@test -d env || virtualenv --python=$(PYTHON) env
	@echo

env-upgrade: env-test
	@echo
	@( \
		. env/bin/activate; \
		pip install pip setuptools wheel twine --upgrade ; \
	)
	@echo

install:
	@echo
	@( \
		. env/bin/activate; \
		pip install -e .[dev]; \
	)
	@echo

env: env-build env-upgrade

dev: env install
	@echo
	@echo ". env/bin/activate"
	@echo

clean:
	@echo
	-rm -fr .eggs/ .cache/ *.egg-info *.egg env
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -type d -name "__pycache__" -delete
	-rm -fr .pytest_cache/ .mypy_cache/ test_result/ .coverage
	-rm -fr build/ dist/ *.tar.gz
	@echo

# ToDo: set development status classifier
version-bump:
	@( \
		. env/bin/activate; \
		python -c 'from datetime import date; f=open("{{ cookiecutter.package }}/__init__.py"); start ,_ , end = f.read().partition("\n__version__ = \""); f.close(); version, _, end = end.partition("\"\n"); td = date.today(); y,m,v = map(int, version.split(".")); ver = f"{y}.{m}.{v+1}" if td.year == y and td.month == m else f"{td.year}.{td.month}.1"; f=open("{{ cookiecutter.package }}/__init__.py", "w"); f.write(f"{start}\n__version__ = \"{ver}\"\n{end}");' ;\
		python -c 'from datetime import date; f=open("LICENSE"); start ,_ , end = f.read().partition("\nCopyright (c) "); f.close(); year, _, end = end.partition(" "); td = date.today(); f=open("LICENSE", "w"); f.write(f"{start}\nCopyright (c) {date.today().year} {end}");' ;\
	)

build-dist:
	@echo
	@( \
		. env/bin/activate; \
		python setup.py sdist bdist_wheel; \
	)
	@echo


twine-check:
	@echo
	@( \
		. env/bin/activate; \
		twine check dist/* ; \
	)
	@echo


twine-upload:
	@echo
	@( \
		. env/bin/activate; \
		twine upload --config-file $(HOME)/.pypirc dist/* ; \
	)
	@echo
