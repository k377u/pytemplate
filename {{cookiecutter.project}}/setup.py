import pkg_resources
import setuptools

pkg_resources.require("setuptools>=61.0.0")
setuptools.setup(
    packages=setuptools.find_packages(exclude=["*.tests", "*.tests.*"]),
    package_data={"": ["py.typed", "*.pyi"]},
)
